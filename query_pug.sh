#!/bin/bash
output_assays="record_assays.csv"
output_cids="match_drugs.csv"
file_smiles="smiles_encoded_full.csv"
NUM_BATCH=8

prelog="https://pubchem.ncbi.nlm.nih.gov/rest/pug"
operation="/assaysummary"
output="/CSV"
operation_match="/property/canonicalsmiles"
output_match="/CSV"

COUNTER=1
while IFS= read -r line
do
	input="/compound/smiles/$line"
	cid_smiles="$(wget -qO- $prelog$input$operation_match$output_match | sed -E -e 's/.*CanonicalSMILES\"//g')"
	echo $cid_smiles >> "$output_cids"
	if !((COUNTER % NUM_BATCH))
	then
		input="/compound/cid"
		command="wget -O- $prelog$input$operation$output --post-data=cid=$cids"
		echo "$($command | tail -n +2)" >> "$output_assays"
		COUNTER=0
		cids="$(echo $cid_smiles | sed -E -e 's/,.*//g')"
	else
		cids="$cids,$(echo $cid_smiles | sed -E -e 's/,.*//g')"
	fi
	COUNTER=$((COUNTER + 1))
done < "$file_smiles"
